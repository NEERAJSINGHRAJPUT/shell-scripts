#!/bin/bash
if lsof -Pi :8083 -sTCP:LISTEN -t >/dev/null ; then
    echo "port running"
else
    echo "port not running"
fi
