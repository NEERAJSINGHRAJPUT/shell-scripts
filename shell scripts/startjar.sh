#!/bin/bash
cd $(dirname $0)
echo 'Starting latest jar'
ls -t *.jar | head -1
export LATEST_VERSION=$(ls -t *.jar | head -1)
nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
