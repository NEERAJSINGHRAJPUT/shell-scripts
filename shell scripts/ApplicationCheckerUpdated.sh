#!/bin/bash

#jar runner

starter(){
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    nohup java -jar $LATEST_VERSION </dev/null &>/dev/null 
    
}


checker(){

#End points

DAYAMED="205.186.138.55:8083/login"
SURESCRIPT="205.186.138.55:8083/login"
VAUAT="205.186.138.55:8083/login"
VA="205.186.138.55:8083/login"

#test Dayamed
curl -X POST  $DAYAMED
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to Dayamed server'
    kill $(lsof -t -i:8083)
    /root/dayamedLive
    cd /home/seneca/dayamed
    
    starter
      

else
    echo 'API responce returned server running'
   
fi

#test DayamedLive
curl -X POST  $SURESCRIPT
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to Dayamed server'
    kill $(lsof -t -i:8083)
    cd /home/seneca/dayamed
    starter
      

else
    echo 'API responce returned SURESCRIPT server running'
   
fi

#VA LIVE
curl -X POST  $VAUAT
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to Dayamed server'
    kill $(lsof -t -i:8083)
    cd /home/seneca/dayamed
    starter
      

else
    echo 'API responce returned VA UAT server running'
   
fi

#VA UAT(VA PHARMACY)
curl -X POST  $VAUAT
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to Dayamed server'
    kill $(lsof -t -i:8083)
    cd /home/seneca/dayamed
    starter
      

else
    echo 'API responce returned VA UAT server running'
   
fi

#VA TRAINING
curl -X POST  $VA
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to Dayamed server'
    cd /home/seneca/dayamed
    #copy file
    #kill $(lsof -t -i:8083)
    echo "killed jar"
    
    starter
      

else
    echo 'API responce returned VA server running'
   
fi

}





root      4478     1  0 Nov20 ?        00:43:35 java -jar Dayamed-1.0.0-SNAPSHOT-UAT-28OCT.jar  /root/dayamed

root      8729     1  0 Nov25 ?        00:28:24 java -jar VALIVE-1.0.0-SNAPSHOT-22SEP.jar /root/valive

root     11981     1  0 Nov25 ?        00:38:19 java -jar MEDICARE-1.0.0-SNAPSHOT-23NovV2.jar  /root/dayamedLive

root     20163     1  0 May06 ?        13:58:31 java -jar VAPharmacy-1.0.0-TAINING-SNAPSHOT-APR19.jar  /root/vatraining

root     26542     1  0 Nov19 ?        00:56:29 java -jar VAPharmacy-1.0.0-SNAPSHOT_UAT_22SEP.jar /root/vapharmacy
