#!/bin/bash
STABLE_VERSION=""
cd $(dirname $0)
echo 'Starting latest jar'
ls -t *.jar | head -1
export LATEST_VERSION=$(ls -t *.jar | head -1)
nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &

#test

sleep 30s

curl -X POST  192.168.1.69:8083/login
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to server/proxy'
    pkill -9 -f $LATEST_VERSION
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    echo starting $STABLE_VERSION
    nohup java -jar $STABLE_VERSION </dev/null &>/dev/null &
    exit 1
    

else
    echo 'API responce returned server running'
    sed -i 's/STABLE_VERSION=""/STABLE_VERSION='$LATEST_VERSION'/g' $0
    echo Updating the stable jar version to $LATEST_VERSION from $STABLE_VERSION
fi
