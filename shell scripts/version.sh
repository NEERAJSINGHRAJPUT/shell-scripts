#!/bin/bash
echo "checkingout DAYAMED_DEV"
git checkout DAYAMED_DEV
TAG_VER=$(sed -n 's,.*<version>\(.*\)</version>.*,\1,p' pom.xml | head -1)
echo "current version  $TAG_VER"
read -p "Enter release type (major/minor/patch): " VAR1

if [[ "$VAR1" == "major" ]]; then
    echo "setting as major release."
    mvn semver:increment-major
    TAG_VER=$(sed -n 's,.*<version>\(.*\)</version>.*,\1,p' pom.xml | head -1)
    echo "current version  $TAG_VER"
    echo"commiting version change to remote"
    git commit -am "increment version"
    #git push origin
    echo "creating tag and pushing to origin"
    git tag V-$TAG_VER
    git push origin V-$TAG_VER
    
    
    
elif [[ "$VAR1" == "minor" ]]; then
    echo "setting as minor release."
    mvn semver:increment-minor
    TAG_VER=$(sed -n 's,.*<version>\(.*\)</version>.*,\1,p' pom.xml | head -1)
    echo "current version  $TAG_VER"
    echo"commiting version change to remote"
    git commit -am "increment version"
    #git push origin
    echo "creating tag and pushing to origin"
    git tag V-$TAG_VER
    git push origin V-$TAG_VER
    
    

elif [[ "$VAR1" == "patch" ]]; then
    echo "setting patch version."
    mvn semver:increment-patch
    TAG_VER=$(sed -n 's,.*<version>\(.*\)</version>.*,\1,p' pom.xml | head -1)
    echo "current version  $TAG_VER"
    echo"commiting version change to remote"
    git commit -am "increment version"
    #git push origin
    echo "creating tag and pushing to origin"
    git tag V-$TAG_VER
    git push origin V-$TAG_VER
    

else
    echo "wrong type."
fi
