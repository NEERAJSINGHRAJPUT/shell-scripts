#!/bin/bash


#DAYAMED="205.186.138.55:8083/login"
#MEDICARE ="205.186.138.55:8088/login"
#VALIVE="205.186.138.55:8087/login"
#VA UAT="205.186.138.55:8085/login"
#VA="205.186.138.55:8085/login"

#jar runner

# starter(){
#     ls -t *.jar | head -1
#     export LATEST_VERSION=$(ls -t *.jar | head -1)
#     echo starting $LATEST_VERSION
#     nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
    
# }


echo "$(date +%F-%T) ran" >> check.log

#test DAYAMED UAT (Dayamed)
curl -X GET  205.186.138.55:8083/getNewPatientMessage
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to DAYAMED (DAYAMED UAT) server'
    cd /root/dayamed
    echo "$(date +%F-%T) cant connect to DAYAMED (DAYAMED UAT) server" >> serverstatus.log 
    kill $(lsof -t -i:8083)
    echo "killed jar"
    #mv nohup.out
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    ls
    #starter
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
      

else
    echo 'API responce returned DAYAMED (DAYAMED UAT) server running'
   
fi

#test Dayamed Live (MEDICARE)
curl -X GET  205.186.138.55:8088/getNewPatientMessage
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to MEDICARE(DAYAMED LIVE) server'
    cd /root/dayamedLive    
    echo "$(date +%F-%T) cant connect to MEDICARE(DAYAMED LIVE)" >> serverstatus.log
    kill $(lsof -t -i:8088)
    echo "killed jar"
    #mv nohup.out 
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    ls
    #starter
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
      

else
    echo 'API responce returned MEDICARE(DAYAMED LIVE) server running'
    cd /root/dayamedLive
    echo "$(date +%F-%T) medicare running" >> checkmedicare.log

   
fi

#VA LIVE
curl -X GET  205.186.138.55:8087/getNewPatientMessage
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to VA UAT(VA LIVE) server'
    cd /root/valive
    echo "$(date +%F-%T) cant connect to VA UAT(VA LIVE) server" >> serverstatus.log
    kill $(lsof -t -i:8087)
    echo "killed jar"
    #mv nohup.out
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    ls
    #starter
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
      

else
    echo 'API responce returned VA UAT(VA LIVE)  server running'
   
fi

#VA UAT(VA PHARMACY)
curl -X GET  205.186.138.55:8085/getNewPatientMessage
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to  VA UAT(VA PHARMACY) server'
    cd /root/vapharmacy    
    echo "$(date +%F-%T) cant connect to VA UAT(VA PHARMACY) server" >> serverstatus.log
    kill $(lsof -t -i:8085)
    echo "killed jar"    
    #mv nohup.out
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    ls
    #starter
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
      

else
    echo 'API responce returned VA UAT(VA PHARMACY) server running'
   
fi

#VA TRAINING
curl -X GET  205.186.138.55:8086/getNewPatientMessage
if [ "$?" = "7" ]; then 
    echo 'connection refused or cant connect to VA Training server'
    cd /root/vatraining    
    #echo "$(date +%F-%T) cant connect to VA Training server" >> serverstatus.log
    #kill $(lsof -t -i:8086)
    echo "killed jar"
    #mv nohup.out
    mv nohup.out nohup_$(date +%d-%m-%Y).out
    ls
    #starter
    ls -t *.jar | head -1
    export LATEST_VERSION=$(ls -t *.jar | head -1)
    echo starting $LATEST_VERSION
    #nohup java -jar $LATEST_VERSION </dev/null &>/dev/null &
     

else
    echo 'API responce returned VA TRAINING server running'
   
fi






# root      4478     1  0 Nov20 ?        00:43:35 java -jar Dayamed-1.0.0-SNAPSHOT-UAT-28OCT.jar  /root/dayamed   PORT 8083

# root      8729     1  0 Nov25 ?        00:28:24 java -jar VALIVE-1.0.0-SNAPSHOT-22SEP.jar /root/valive   PORT 8087

# root     11981     1  0 Nov25 ?        00:38:19 java -jar MEDICARE-1.0.0-SNAPSHOT-23NovV2.jar  /root/dayamedLive    PORT 8088

# root     20163     1  0 May06 ?        13:58:31 java -jar VAPharmacy-1.0.0-TAINING-SNAPSHOT-APR19.jar  /root/vatraining PORT 8086

# root     26542     1  0 Nov19 ?        00:56:29 java -jar VAPharmacy-1.0.0-SNAPSHOT_UAT_22SEP.jar /root/vapharmacy PORT 8085